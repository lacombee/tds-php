<?php




require_once 'src/Modele/ConnexionBaseDeDonnees.php';
require_once 'src/Modele/Utilisateur.php';

class Trajet
{

    private array $passagers;
    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private ModeleUtilisateur $conducteur;
    private bool $nonFumeur;

    public function __construct(
        ?int              $id,
        string            $depart,
        string            $arrivee,
        DateTime          $date,
        int               $prix,
        ModeleUtilisateur $conducteur,
        bool              $nonFumeur,
        array             $passagers = []
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    public static function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {
        // Créer l'objet Trajet sans les passagers
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            ModeleUtilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );

        // Récupérer et ajouter les passagers au trajet
        $trajet->setPassagers($trajet->recupererPassagers());

        return $trajet;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): ModeleUtilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(ModeleUtilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * Récupérer les passagers du trajet courant
     * @return ModeleUtilisateur[]
     */
    private function recupererPassagers(): array
    {
        // Tableau pour stocker les objets Utilisateur correspondant aux passagers
        $passagers = [];

        // Connexion à la base de données
        $db = ConnexionBaseDeDonnees::getPdo();

        // Requête SQL pour récupérer les passagers du trajet en fonction de l'id du trajet
        $sql = "SELECT u.login, u.nom, u.prenom 
                FROM passager pt
                INNER JOIN utilisateur u ON pt.passagerLogin = u.login
                WHERE pt.trajetId = :trajet_id";

        // Préparer la requête
        $stmt = $db->prepare($sql);
        // Lier le paramètre du trajet_id
        $stmt->bindParam(':trajet_id', $this->id, PDO::PARAM_INT);
        // Exécuter la requête
        $stmt->execute();

        // Parcourir les résultats et créer des objets Utilisateur pour chaque passager
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $passager = new ModeleUtilisateur($row['nom'], $row['prenom'], $row['login']);
            $passagers[] = $passager;
        }

        return $passagers;
    }

    public static function recupererTrajets(): array
    {
        // Connexion à la base de données
        $db = ConnexionBaseDeDonnees::getPdo();

        // Tableau qui va contenir les trajets
        $trajets = [];

        // Requête SQL pour récupérer tous les trajets
        $sql = "SELECT * FROM trajet";

        // Préparation et exécution de la requête
        $stmt = $db->prepare($sql);
        $stmt->execute();

        // Récupérer les résultats sous forme de tableau associatif
        $resultats = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Boucler sur les résultats et construire des objets Trajet
        foreach ($resultats as $trajetTableau) {
            // Construire un trajet à partir du tableau SQL
            $trajet = self::construireDepuisTableauSQL($trajetTableau);

            // Récupérer les passagers et les associer au trajet
            $trajet->setPassagers($trajet->recupererPassagers());

            // Ajouter le trajet au tableau des trajets
            $trajets[] = $trajet;
        }

        // Retourner le tableau des trajets
        return $trajets;
    }
}
