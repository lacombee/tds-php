<?php
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use \App\Covoiturage\Modele\DataObject\Utilisateur;
$para = UtilisateurRepository::recupererUtilisateur();
UtilisateurRepository::supprimerParLogin($para->getLogin());
echo "L’utilisateur de login {$para->getLogin()} a bien été supprimé";

require DIR . "/liste.php";