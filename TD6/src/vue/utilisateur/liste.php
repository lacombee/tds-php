<!DOCTYPE html>
<html lang="fr">


<body>
<h2>Liste des utilisateurs</h2>

<ul>
    <?php if (!empty($utilisateurs)) : ?>
        <?php foreach ($utilisateurs as $utilisateur): ?>
            <li>
                <a href="?action=afficherDetail&login=<?= rawurlencode($utilisateur->getLogin()) ?>">
                    <?= htmlspecialchars($utilisateur->getLogin()) ?>
                </a>
                - <?= htmlspecialchars($utilisateur->getPrenom()) ?> <?= htmlspecialchars($utilisateur->getNom()) ?>
            </li>
        <?php endforeach; ?>
    <?php else : ?>
        <li>Aucun utilisateur trouvé.</li>
    <?php endif; ?>
</ul>

<!-- Lien vers le formulaire de création -->
<a href="routeur.php?action=afficherFormulaireCreation">Créer un utilisateur</a>
<a href="routeur.php?action=afficherutilisateurSupprime">Suprimer un utilisateur</a>
<a href="routeur.php?action=afficherformulaireMiseAJour">Mettre a jour un utilisateur</a>

</body>
</html>
