<h1>Mettre a jour un nouvel utilisateur</h1>

<?php
$utilisateur = Utilisateur::recupererUtilisateur()
?>

<!-- Formulaire de création d'utilisateur -->
<form method="GET" action="../../../web/controleurFrontal.php">
    <!-- Champ caché pour identifier l'action -->
    <input type='hidden' name='action' value='mettreAJour'>

    <fieldset>
        <legend>Informations utilisateur :</legend>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom :</label>
            <input class="InputAddOn-field" type="text" name="nom" id="nom_id" placeholder="Leblanc" required />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom :</label>
            <input class="InputAddOn-field" type="text" name="prenom" id="prenom_id" placeholder="Juste" required />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login :</label>
            <input value="readonly" class="InputAddOn-field" type="text" name="login" id="login_id" placeholder="leblancj" required />
        </p>

        <p class="InputAddOn">
            <input type="submit" value="mettreAJour" />
        </p>
    </fieldset>
</form>
