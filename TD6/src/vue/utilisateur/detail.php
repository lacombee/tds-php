<!DOCTYPE html>
<html>


<body>
<?php if (isset($utilisateur)) : ?>
    <h2>Détails de l'utilisateur</h2>
    <p><strong>Login :</strong> <?= htmlspecialchars($utilisateur->getLogin()) ?></p>
    <p><strong>Prénom :</strong> <?= htmlspecialchars($utilisateur->getPrenom()) ?></p>
    <p><strong>Nom :</strong> <?= htmlspecialchars($utilisateur->getNom()) ?></p>
    <p><strong>Email :</strong> <?= htmlspecialchars($utilisateur->getEmail()) ?></p>
    <!-- Ajoutez d'autres détails si nécessaires -->
<?php else : ?>
    <p>Aucun utilisateur trouvé.</p>
<?php endif; ?>
</body>


</html>
