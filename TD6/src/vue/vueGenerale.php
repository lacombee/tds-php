
<!DOCTYPE html>

<html>
<head>
    <link rel="stylesheet" href="../ressources/css/css.css">
    <meta charset="UTF-8">
    <title><?php
        /**
         * @var string $titre
         */
        /**
         * @var string $cheminCorpsVue
         */
        echo $titre; ?></title>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
        </ul>
    </nav>


</head>
<body>
<header>
    <nav>
        <!-- Votre menu de navigation ici -->
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de ...
    </p>
</footer>
</body>
</html>

