<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\Utilisateur;

class UtilisateurRepository
{

    public static function recupererUtilisateur(): array
    {

        $utilisateurs = [];

        $pdo = ConnexionBaseDeDonnees::getPdo();

        $sql = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($sql);

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateur = self::construireDepuisTableauSQL($utilisateurFormatTableau);
            $utilisateurs[] = $utilisateur;

        }

        return $utilisateurs;
    }


    public static function recupererUtilisateurParLogin($login)
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $query = $pdo->prepare('SELECT * FROM utilisateur WHERE login = :login');
        $query->execute(['login' => $login]);

        if ($query->rowCount() === 0) {
            return false; // Aucune correspondance trouvée
        }

        return $query->fetch(PDO::FETCH_ASSOC); // Renvoie un tableau associatif
    }


    public static function ajouter($utilisateur)
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        // Vérifiez si l'utilisateur existe déjà
        $query = $pdo->prepare('SELECT COUNT(*) FROM utilisateur WHERE login = :login');
        $query->execute(['login' => $utilisateur->login]); // Utilisez la bonne propriété
        $count = $query->fetchColumn();

        if ($count > 0) {
            throw new Exception("L'utilisateur existe déjà.");
        }

        // Insérer l'utilisateur si il n'existe pas déjà
        $query = $pdo->prepare('INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)');
        $query->execute(['login' => $utilisateur->login, 'nom' => $utilisateur->nom, 'prenom' => $utilisateur->prenom]);
    }


    public static function supprimerParLogin($login)
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $query = $pdo->prepare('delete from utilisateur where login = :login');
        $query->execute(['login' => $login]);
    }

    public static function mettreAJour(Utilisateur $utilisateur){
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $query = $pdo->prepare('UPDATE utilisateur set nom = :nom, prenom = :prenom where login = :login');
        $query = $query->execute(['login' => $utilisateur->login, 'nom' => $utilisateur->nom, 'prenom' => $utilisateur->prenom]);
        $count = $query->fetch();
        if ($count > 0) {
            throw new Exception("L'utilisateur existe déjà.");
        }
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['prenom'],
            $utilisateurFormatTableau['nom'],);
    }
}