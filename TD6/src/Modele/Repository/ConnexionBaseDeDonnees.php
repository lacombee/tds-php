<?php


namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Configuration\ConfigurationBaseDeDonnees;
use \PDO as PDO;
#require_once __DIR__ . '/../Configuration/ConfigurationBaseDeDonnees.php';

class ConnexionBaseDeDonnees
{

    private PDO $pdo;
    private static ?ConnexionBaseDeDonnees $instance = null;


    private function __construct()
    {
        //$ConnexionBaseDeDonnees = new ConfigurationBaseDeDonnees();
        $login = ConfigurationBaseDeDonnees::getLogin();
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $port = ConfigurationBaseDeDonnees::getPort();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        $motDePasse = ConfigurationBaseDeDonnees::getMotDePasse();
        $this->pdo = new PDO("mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees", $login, $motDePasse,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    private static function getInstance(): ConnexionBaseDeDonnees
    {
        // L'attribut statique $instance s'obtient avec la syntaxe ConnexionBaseDeDonnees::$instance
        if (is_null(ConnexionBaseDeDonnees::$instance))
            // Appel du constructeur
            ConnexionBaseDeDonnees::$instance = new ConnexionBaseDeDonnees();
        return ConnexionBaseDeDonnees::$instance;
    }

    public static function getPdo(): PDO
    {
        return ConnexionBaseDeDonnees::getInstance()->pdo;
    }


}
