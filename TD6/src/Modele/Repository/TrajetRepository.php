<?php

namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
class TrajetRepository
{

    public static function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {
        // Créer l'objet Trajet sans les passagers
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            UtilisateurRepository::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );

        // Récupérer et ajouter les passagers au trajet
        $trajet->setPassagers($trajet->recupererPassagers());

        return $trajet;
    }

    private function recupererPassagers(): array
    {
        // Tableau pour stocker les objets Utilisateur correspondant aux passagers
        $passagers = [];

        // Connexion à la base de données
        $db = ConnexionBaseDeDonnees::getPdo();

        // Requête SQL pour récupérer les passagers du trajet en fonction de l'id du trajet
        $sql = "SELECT u.login, u.nom, u.prenom 
                FROM passager pt
                INNER JOIN utilisateur u ON pt.passagerLogin = u.login
                WHERE pt.trajetId = :trajet_id";

        // Préparer la requête
        $stmt = $db->prepare($sql);
        // Lier le paramètre du trajet_id
        $stmt->bindParam(':trajet_id', $this->id, PDO::PARAM_INT);
        // Exécuter la requête
        $stmt->execute();

        // Parcourir les résultats et créer des objets Utilisateur pour chaque passager
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $passager = new Utilisateur($row['nom'], $row['prenom'], $row['login']);
            $passagers[] = $passager;
        }

        return $passagers;
    }

    public static function recupererTrajets(): array
    {
        // Connexion à la base de données
        $db = ConnexionBaseDeDonnees::getPdo();

        // Tableau qui va contenir les trajets
        $trajets = [];

        // Requête SQL pour récupérer tous les trajets
        $sql = "SELECT * FROM trajet";

        // Préparation et exécution de la requête
        $stmt = $db->prepare($sql);
        $stmt->execute();

        // Récupérer les résultats sous forme de tableau associatif
        $resultats = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Boucler sur les résultats et construire des objets Trajet
        foreach ($resultats as $trajetTableau) {
            // Construire un trajet à partir du tableau SQL
            $trajet = self::construireDepuisTableauSQL($trajetTableau);

            // Récupérer les passagers et les associer au trajet
            $trajet->setPassagers($trajet->recupererPassagers());

            // Ajouter le trajet au tableau des trajets
            $trajets[] = $trajet;
        }

        // Retourner le tableau des trajets
        return $trajets;
    }


}