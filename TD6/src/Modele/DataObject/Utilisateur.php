<?php

namespace App\Covoiturage\Modele\DataObject;

#require_once __DIR__ . "/ConnexionBaseDeDonnees.php";

class Utilisateur
{
    private string $nom;
    private string $prenom;
    private string $login;

    public function __construct(string $nom, string $prenom, string $login)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->setLogin($login);
    }

    public function setLogin(string $login): void
    {
        $this->login = strlen($login) > 64 ? substr($login, 0, 64) : $login;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }










    /*public function __toString(): string {
        return "Utilisateur: " . $this->prenom . " " . $this->nom . " (login: " . $this->login . ")";
    }
    */
}

?>