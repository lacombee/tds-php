<?php

use Configuration\ConfigurationBaseDeDonnees;

require_once 'ConfigurationBaseDeDonnees.php';
class ConnexionBaseDeDonnees{

    private PDO $pdo;
    private static ?Modele\ConnexionBaseDeDonnees $instance = null;


    private function __construct() {
        //$ConnexionBaseDeDonnees = new ConfigurationBaseDeDonnees();
        $login = ConfigurationBaseDeDonnees::getLogin();
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $port = ConfigurationBaseDeDonnees::getPort();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        $motDePasse = ConfigurationBaseDeDonnees::getMotDePasse();
        $this->pdo = new PDO("mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees", $login, $motDePasse,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    private static function getInstance() : Modele\ConnexionBaseDeDonnees {
        // L'attribut statique $instance s'obtient avec la syntaxe ConnexionBaseDeDonnees::$instance
        if (is_null(Modele\ConnexionBaseDeDonnees::$instance))
            // Appel du constructeur
            Modele\ConnexionBaseDeDonnees::$instance = new Modele\ConnexionBaseDeDonnees();
        return Modele\ConnexionBaseDeDonnees::$instance;
    }

    public static function getPdo(): PDO {
        return Modele\ConnexionBaseDeDonnees::getInstance()->pdo;
    }



}
