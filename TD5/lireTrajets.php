<?php

// Inclure les fichiers nécessaires
require_once __DIR__ . '/Trajet.php';
require_once __DIR__ . '/src/Modele/ModeleUtilisateur.php';

// Récupérer tous les trajets
$listeDesTrajets = Trajet::recupererTrajets();

// Vérifier s'il y a des trajets
if (empty($listeDesTrajets)) {
    echo "<p>Aucun trajet n'a été trouvé.</p>";
} else {
    // Boucler sur chaque trajet pour l'afficher
    foreach ($listeDesTrajets as $trajet) {
        echo $trajet; // Appel à __toString() pour afficher les infos du trajet

        // Récupérer la liste des passagers du trajet
        $passagers = $trajet->getPassagers();

        // Afficher la liste des passagers
        if (empty($passagers)) {
            echo "<p>Aucun passager n'est enregistré pour ce trajet.</p>";
        } else {
            echo "<h4>Passagers :</h4><ul>";
            foreach ($passagers as $passager) {
                echo "<li>" . $passager->getPrenom() . " " . $passager->getNom() . " (login : " . $passager->getLogin() . ")</li>";
            }
            echo "</ul>";
        }
    }
}
