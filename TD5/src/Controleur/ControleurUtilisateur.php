<?php


namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Utilisateur;

#require_once __DIR__ . '/../Modele/Utilisateur.php'; // Chargement du modèle

class ControleurUtilisateur
{

    public static function afficherListe(): void
    {
        $utilisateurs = Utilisateur::recupererUtilisateur(); // Récupération des utilisateurs
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]); // Affichage de la vue liste
    }

    public static function afficherDetail(): void
    {
        $login = $_GET['login'] ?? null; // Récupérer le login depuis l'URL

        if ($login === null) {
            self::afficherVue('erreur.php'); // Pas de login fourni
            return;
        }

        $utilisateurFormatTableau = Utilisateur::recupererUtilisateurParLogin($login);

        if ($utilisateurFormatTableau === false) {
            self::afficherVue('erreur.php'); // Utilisateur non trouvé
            return;
        }

        // Si l'utilisateur est trouvé, le construire et afficher la vue
        $utilisateur = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        self::afficherVue('detail.php', ['utilisateur' => $utilisateur]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php' , ["titre" => "Création utisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]); // Affiche la vue formulaire
    }

    public static function creerDepuisFormulaire(): void
    {
        // Récupérer les données du formulaire via $_GET
        $nom = $_GET['nom'] ?? null;
        $prenom = $_GET['prenom'] ?? null;
        $login = $_GET['login'] ?? null;

        // Vérifier que toutes les données sont présentes
        if ($nom && $prenom && $login) {
            // Créer une nouvelle instance de Utilisateur
            $utilisateur = new Utilisateur($login, $nom, $prenom);

            // Ajouter l'utilisateur dans la base de données
            $utilisateur->ajouter();

            // Afficher la liste des utilisateurs mise à jour
            self::afficherListe();
        } else {
            // Si des données sont manquantes, afficher une vue d'erreur
            self::afficherVue('utilisateur/erreur.php');
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}
