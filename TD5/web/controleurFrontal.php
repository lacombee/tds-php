<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;

#require_once __DIR__ . '/../src/Controleur/ControleurUtilisateur.php';
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');




// Récupération de l'action dans l'URL
$action = $_GET['action'] ?? 'afficherListe';

// Gestion des actions
switch ($action) {
    case 'afficherListe':
        ControleurUtilisateur::afficherListe();
        break;

    case 'afficherDetail':
        ControleurUtilisateur::afficherDetail();
        break;

    case 'afficherFormulaireCreation':
        ControleurUtilisateur::afficherFormulaireCreation();
        break;

    case 'creerDepuisFormulaire':
        ControleurUtilisateur::creerDepuisFormulaire();
        break;

    default:
        ControleurUtilisateur::afficherListe();
        break;
}
