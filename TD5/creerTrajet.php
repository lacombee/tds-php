<?php
// Vérifier si les données ont été envoyées via POST


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Vérifier si le tableau $_POST n'est pas vide
    if (!empty($_POST)) {
        // Récupérer les données du formulaire
        $dateString = $_POST['date'] ?? null; // Récupérer la date en string
        $conducteurLogin = $_POST['conducteurLogin'] ?? 'Non fourni'; // Login du conducteur
        $lieuDepart = $_POST['lieuDepart'] ?? 'Non fourni';
        $lieuArrivee = $_POST['lieuArrivee'] ?? 'Non fourni';
        $nbPlaces = $_POST['nbPlaces'] ?? 0;
        $nonFumeur = isset($_POST['nonFumeur']) ? 1 : 0; // Booléen nonFumeur

        // Validation basique pour vérifier que les données essentielles sont fournies
        if (!$dateString || !$conducteurLogin || !$lieuDepart || !$lieuArrivee || !$nbPlaces) {
            echo "<p>Veuillez remplir tous les champs obligatoires.</p>";
            exit;
        }

        // Transformer la date string en objet DateTime
        try {
            $date = new DateTime($dateString);
        } catch (Exception $e) {
            echo "<p>Date invalide : {$e->getMessage()}</p>";
            exit;
        }

        // Inclure les classes nécessaires
        require_once 'Utilisateur.php';
        require_once 'Trajet.php';

        // Récupérer l'objet Utilisateur correspondant au conducteur à partir du login
        // Dans un cas réel, vous chercheriez cet utilisateur dans la base de données
        $conducteur = ModeleUtilisateur::findByLogin($conducteurLogin);
        if (!$conducteur) {
            echo "<p>Conducteur non trouvé pour le login : $conducteurLogin</p>";
            exit;
        }

        // Créer un objet Trajet avec les données reçues
        $trajet = new Trajet(
            null, // id du trajet (null car il sera généré automatiquement)
            $date,
            $conducteur, // objet Utilisateur correspondant au conducteur
            $lieuDepart,
            $lieuArrivee,
            $nbPlaces,
            $nonFumeur // Booléen (1 = non-fumeur, 0 = fumeur)
        );

        // Appeler la méthode ajouter() pour enregistrer le trajet en base de données
        if ($trajet->ajouter()) {
            echo "<p>Trajet ajouté avec succès !</p>";
        } else {
            echo "<p>Une erreur est survenue lors de l'ajout du trajet.</p>";
        }
    } else {
        echo "<p>Aucune donnée reçue.</p>";
    }
} else {
    echo "<p>Le formulaire n'a pas été soumis avec la méthode POST.</p>";
}
?>
