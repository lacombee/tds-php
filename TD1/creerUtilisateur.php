<?php
// Vérifier si les données ont été envoyées via POST
use Modele\ModeleUtilisateur;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Vérifier si le tableau $_POST n'est pas vide
    if (!empty($_POST)) {
        // Récupérer les données du formulaire
        $nom = $_POST['nom'] ?? 'Non fourni';
        $prenom = $_POST['prenom'] ?? 'Non fourni';
        $login = $_POST['login'] ?? 'Non fourni';

        // Inclure la classe Utilisateur
        require_once 'Utilisateur.php';

        // Créer un objet Utilisateur avec les données reçues
        $utilisateur = new ModeleUtilisateur($nom, $prenom, $login);

        // Afficher les informations de l'utilisateur en utilisant __toString()
        echo "<h1>Informations de l'utilisateur :</h1>";
        echo $utilisateur; // Appelle implicitement __toString()
    } else {
        echo "<p>Aucune donnée reçue.</p>";
    }
} else {
    echo "<p>Le formulaire n'a pas été soumis avec la méthode POST.</p>";
}
?>
