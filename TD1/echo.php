<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;


        $prenom = "Marc";

        $utilisateurs = [
            [
                "nom" => "Leblanc",
                "prenom" => "Juste",
                "login" => "leblancj"
            ],
            [
                "nom" => "Dupont",
                "prenom" => "Marie",
                "login" => "dupontm"
            ],
            [
                "nom" => "Martin",
                "prenom" => "Paul",
                "login" => "martinp"
            ]
        ];

        // Utiliser var_dump pour vérifier le contenu du tableau $utilisateurs


        // Commencer la création du contenu HTML
        $html = "<h1>Liste des utilisateurs :</h1>";

        // Vérifier si la liste est vide
        if (empty($utilisateurs)) {
            $html .= "<p>Il n’y a aucun utilisateur.</p>";
        } else {
            $html .= "<ul>";
            // Boucle pour ajouter chaque utilisateur à la liste HTML
            foreach ($utilisateurs as $utilisateur) {
                $html .= "<li>Utilisateur " . $utilisateur['prenom'] . " " . $utilisateur['nom'] . " de login " . $utilisateur['login'] . "</li>";
            }
            $html .= "</ul>";
        }
        echo $html;
        ?>




    </body>
</html> 