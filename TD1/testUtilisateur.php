



<!DOCTYPE html>
<html>
    <head>
        <title> C lme titre </title>
        <meta charset="utf-8" />
    </head>

    <body>

    <?php


    use Modele\ModeleUtilisateur;

    require_once 'Utilisateur.php';

    // Créer des objets Utilisateur
    $utilisateur1 = new ModeleUtilisateur("Leblanc", "Juste", "unloginextrêmementlongquidepasse64caractèresetqui-seratrimé");
    $utilisateur2 = new ModeleUtilisateur("Dupont", "Marie", "dupontm");
    $utilisateur3 = new ModeleUtilisateur("Martin", "Paul", "martinp");

    echo $utilisateur1; // Affichera l'utilisateur 1
    echo "<br>"; // Nouvelle ligne
    echo $utilisateur2; // Affichera l'utilisateur 2
    echo "<br>"; // Nouvelle ligne
    echo $utilisateur3; // Affichera l'utilisateur 3

    ?>
    </body>
</html>
