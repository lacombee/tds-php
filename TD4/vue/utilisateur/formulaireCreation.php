<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire de création d'utilisateur</title>
</head>
<body>
<h1>Créer un nouvel utilisateur</h1>

<!-- Formulaire de création d'utilisateur -->
<form method="GET" action="routeur.php">
    <!-- Champ caché pour identifier l'action -->
    <input type="hidden" name="action" value="creerDepuisFormulaire">

    <fieldset>
        <legend>Informations utilisateur :</legend>

        <p>
            <label for="nom_id">Nom :</label>
            <input type="text" name="nom" id="nom_id" placeholder="Leblanc" required />
        </p>

        <p>
            <label for="prenom_id">Prénom :</label>
            <input type="text" name="prenom" id="prenom_id" placeholder="Juste" required />
        </p>

        <p>
            <label for="login_id">Login :</label>
            <input type="text" name="login" id="login_id" placeholder="leblancj" required />
        </p>

        <p>
            <input type="submit" value="Créer l'utilisateur" />
        </p>
    </fieldset>
</form>
</body>
</html>
