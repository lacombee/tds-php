<?php

use Modele\ModeleUtilisateur;

require_once ('../Modele/ModeleUtilisateur.php'); // Chargement du modèle

class ControleurUtilisateur {

    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateur(); // Récupération des utilisateurs
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]); // Affichage de la vue liste
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'] ?? null; // Récupérer le login depuis l'URL

        if ($login === null) {
            self::afficherVue('utilisateur/erreur.php'); // Pas de login fourni
            return;
        }

        $utilisateurFormatTableau = ModeleUtilisateur::recupererUtilisateurParLogin($login);

        if ($utilisateurFormatTableau === false) {
            self::afficherVue('utilisateur/erreur.php'); // Utilisateur non trouvé
            return;
        }

        // Si l'utilisateur est trouvé, le construire et afficher la vue
        $utilisateur = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('utilisateur/formulaireCreation.php'); // Affiche la vue formulaire
    }

    public static function creerDepuisFormulaire() : void {
        // Récupérer les données du formulaire via $_GET
        $nom = $_GET['nom'] ?? null;
        $prenom = $_GET['prenom'] ?? null;
        $login = $_GET['login'] ?? null;

        // Vérifier que toutes les données sont présentes
        if ($nom && $prenom && $login) {
            // Créer une nouvelle instance de Utilisateur
            $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);

            // Ajouter l'utilisateur dans la base de données
            $utilisateur->ajouter();

            // Afficher la liste des utilisateurs mise à jour
            self::afficherListe();
        } else {
            // Si des données sont manquantes, afficher une vue d'erreur
            self::afficherVue('utilisateur/erreur.php');
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
}
