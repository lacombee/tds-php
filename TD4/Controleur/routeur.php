<?php

use Controleur\ControleurUtilisateur;

require_once 'ControleurUtilisateur.php';

// Récupération de l'action dans l'URL
$action = $_GET['action'] ?? 'afficherListe';

// Gestion des actions
switch ($action) {
    case 'afficherListe':
        ControleurUtilisateur::afficherListe();
        break;

    case 'afficherDetail':
        ControleurUtilisateur::afficherDetail();
        break;

    case 'afficherFormulaireCreation':
        ControleurUtilisateur::afficherFormulaireCreation();
        break;

    case 'creerDepuisFormulaire':
        ControleurUtilisateur::creerDepuisFormulaire();
        break;

    default:
        ControleurUtilisateur::afficherListe();
        break;
}
