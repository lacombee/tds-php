<?php

use Modele\ConnexionBaseDeDonnees;

require_once "ConnexionBaseDeDonnees.php";
class ModeleUtilisateur {
    private string $nom;
    private string $prenom;
    private string $login;

    public function __construct(string $nom, string $prenom, string $login) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->setLogin($login);
    }

    public function setLogin(string $login): void {
        $this->login = strlen($login) > 64 ? substr($login, 0, 64) : $login;
    }

    public function getLogin(): string {
        return $this->login;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function getPrenom(): string {
        return $this->prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Modele\ModeleUtilisateur {
        return new Modele\ModeleUtilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['prenom'],
            $utilisateurFormatTableau['nom'],);
    }

    public static function recupererUtilisateur() : array{

        $utilisateurs= [];

        $pdo = ConnexionBaseDeDonnees::getPdo();

        $sql = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($sql);

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateur = self::construireDepuisTableauSQL($utilisateurFormatTableau);
            $utilisateurs[] = $utilisateur;

        }

        return $utilisateurs;
    }


    public function ajouter() {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        // Vérifiez si l'utilisateur existe déjà
        $query = $pdo->prepare('SELECT COUNT(*) FROM utilisateur WHERE login = :login');
        $query->execute(['login' => $this->login]); // Utilisez la bonne propriété
        $count = $query->fetchColumn();

        if ($count > 0) {
            throw new Exception("L'utilisateur existe déjà.");
        }

        // Insérer l'utilisateur si il n'existe pas déjà
        $query = $pdo->prepare('INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)');
        $query->execute(['login' => $this->login, 'nom' => $this->nom, 'prenom' => $this->prenom]);
    }




    public static function recupererUtilisateurParLogin($login) {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $query = $pdo->prepare('SELECT * FROM utilisateur WHERE login = :login');
        $query->execute(['login' => $login]);

        if ($query->rowCount() === 0) {
            return false; // Aucune correspondance trouvée
        }

        return $query->fetch(PDO::FETCH_ASSOC); // Renvoie un tableau associatif
    }







    /*public function __toString(): string {
        return "Utilisateur: " . $this->prenom . " " . $this->nom . " (login: " . $this->login . ")";
    }
    */
}
?>