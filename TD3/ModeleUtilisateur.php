<?php

use Modele\ConnexionBaseDeDonnees;

class ModeleUtilisateur {
    private string $nom;
    private string $prenom;
    private string $login;

    public function __construct(string $nom, string $prenom, string $login) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->setLogin($login);
    }

    public function setLogin(string $login): void {
        $this->login = strlen($login) > 64 ? substr($login, 0, 64) : $login;
    }

    public function getLogin(): string {
        return $this->login;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function getPrenom(): string {
        return $this->prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Modele\ModeleUtilisateur {
        return new Modele\ModeleUtilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['prenom'],
            $utilisateurFormatTableau['nom'],);
    }

    public static function recupererUtilisateur() : array{

        $utilisateurs= [];

        $pdo = ConnexionBaseDeDonnees::getPdo();

        $sql = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($sql);

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateur = self::construireDepuisTableauSQL($utilisateurFormatTableau);
            $utilisateurs[] = $utilisateur;

        }

        return $utilisateurs;
    }


        public function ajouter() : void {

            // Requête SQL d'insertion
            $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:logintag, :nomtag, :prenomtag)";

            // Préparation de la requête
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "logintag" => $this->login,
                "nomtag" => $this->nom,
                "prenomtag" => $this->prenom,
            );

            // Exécution de la requête
            $pdoStatement->execute($values);

            $utilisateurFormatTableau = $pdoStatement;
        }


    public static function recupererUtilisateurParLogin(string $login) : Modele\ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        return Modele\ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }




    public function __toString(): string {
        return "Utilisateur: " . $this->prenom . " " . $this->nom . " (login: " . $this->login . ")";
    }
}
?>